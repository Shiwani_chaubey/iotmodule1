# **Industrial IoT**

![](extras/1.png.gif)

## Industrial IoT as the leverage and reality of IoT in a context of industrial transformation.
Industrial IoT in the earlier mentioned sense was mainly used to make a distinction between the use cases, actual usage and specific technologies as leveraged for initially mainly manufacturing (smart factory) and, later, other industries on one hand and enterprise IoT and consumer IoT applications on the other.
Also there are basic revolution in the industry of IOT:

![](extras/2.jpg)

## Industry 3.0:
Data is stored in database and represented in excels. Also in industry 3.0 communication protocols, all these protocols are optimized for sending data to a central server inside the factory. These protocols are used by sensors to send data to PLC’s and these protocols are called as fieldbus.

![](extras/3.png)
![](extras/4.jpg)

## Industry 4.0:
Industry 4.0 is industry 3.0 devices connected to the internet (IOT) and when these devices is connected to internet they will send the data. All the protocols of industry 4.0 are optimised for sending data to cloud for data analytics.
There are various problems with industry 4.0 upgrades and some problems are mentioned so that the factory owners face and have to aware of:
Problem 1. Cost: Industry 3.0 devices are very expensive as a factory owner I don’t want to switch to industry 4.0 devices because they are expensive.
Problem 2. Downtime: changing hardware means a big factory downtime. I don’t want to shut down my factory to upgrade devices.
Problem 3. Reliability:  I don’t want to insert in devices which are unproven and unreliable.

![](extras/5.png)
![](extras/6.jpg)

## Converting Indusry 3.0 devices to Industry 4.0:
To convert a 3.0 to 4.0, a library that helps, get data from industry 3.0 devices and send to industry 4.0 cloud.
There is a roadmap for making own industrial IOT protocol by the use of industry 3.0 and industry 4.0:
Step1: Identify most popular industry 3.0 devices
Step2: Study protocols that these devices communicate
Step3: Get data from the industry 3.0 devices
Step4: Send the data to cloud for industry 4.0

![](extras/7.jpg)
